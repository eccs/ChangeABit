class Board{
	int[] bits;
	color[] colors;
	int[] notes;
	int currentAddr;

	int NCELLS;
	int NROWS;
	int NCOLS;
	int NADDRBITS;// Number of address bits
	int NCOLORBITS; // Number of color bits
	int NBITS;

	int totalWidth;
	int totalHeight;

	Board(){
		NROWS = 8;
		NCOLS = NROWS;
		NCELLS = NROWS * NCOLS;
		NADDRBITS = 6;
		NCOLORBITS = 3;
		NBITS = NADDRBITS + NCOLORBITS;

		bits = new int[NBITS];
		colors = new color[NCELLS];
		notes = new int[NCELLS];

		for(int i=0; i<NCELLS; i++){
			colors[i] = color(0);	
		}
		currentAddr = 0;

		totalWidth = 600;
		totalHeight = totalWidth;

	}

	private int bitsToInt(int[] b){
		int n = 0;
		int p = 0;
		for(int i=b.length-1; i>=0; i--){
			n += b[i] * pow(2,p);
			p++;
		}
		return n;

	}

	private int addressFromBits(int[] b){
		int[] a = new int[NADDRBITS];
		for(int i=0; i<NADDRBITS; i++){
			a[i] = b[i];
		}
		return bitsToInt(a);
	}

	private color colorFromBits(int[] b){
		int [] c = new int[NCOLORBITS];
		for(int i=0; i<NCOLORBITS; i++){
			c[i] = b[i+NADDRBITS];
		}
		// Hardcoding the color conversion
		float re = map(c[0], 0, 1, 0, 255);
		float gr = map(c[1], 0, 1, 0, 255);
		float bl = map(c[2], 0, 1, 0, 255);

		return color(re,gr,bl);

	}

	private int colorIntFromBits(int [] b){
		int [] c = new int[NCOLORBITS];
		for(int i=0; i<NCOLORBITS; i++){
			c[i] = b[i+NADDRBITS];
		}
		return bitsToInt(c);
	}

	public int[] bitsToThreeNumbers(){
		int a = addressFromBits(bits);
		int c = colorIntFromBits(bits);
		return new int[]{(a>>3)&0x7, a&0x7, c};
	}



	void clearColors(){
		for(int i=0; i<NCELLS; i++){
			colors[i] = color(0);	
		}
	}

	void setBits(int[] b){
		for(int i=0; i<NBITS; i++){
			bits[i] = b[i];
		}
	}

	void update(){
		color c = colorFromBits(bits);
		int addr = addressFromBits(bits);
		currentAddr = addr;
		colors[addr] = c;
		notes[addr] = colorIntFromBits(bits);
	}

	void draw(){
		background(0);
		pushMatrix();
		translate((width-totalWidth)/2,(height-totalHeight)/2);
		int row, col, x,y, w, h;

		w = totalWidth/NCOLS;
		h = totalHeight/NROWS;

		noStroke();
		int note;
		for(int i=0; i<NCELLS; i++){
			row = i/NCOLS;		
			col = i%NCOLS;
			x = col*w;
			y = row*h;
			note = notes[i];
			//fill(colors[i]);
			fill( (note>>2) * 255);
			rect(x,y,w,h);

			fill( ((note>>1)&1) * 255);
			rect(x+w/5, y+h/5, w*3/5, h*3/5);

			fill( ((note>>0)&1) * 255);
			rect(x+w*2/5, y+h*2/5, w/5, h/5);
		}

		int i = currentAddr;
		row = i/NCOLS;		
		col = i%NCOLS;
		x = col*w;
		y = row*h;
		strokeWeight(3);
		int t = millis()%3000;
		float a = sin(t*TWO_PI/1000);
		stroke(map(a,-1,1,0,255));
		noFill();
		rect(x,y,w,h);

		popMatrix();
	}

	/*
	String getNote(int i){
		int n = notes[i];
		String [] notes = {"D3","E3","F3"

	}
	*/


}
