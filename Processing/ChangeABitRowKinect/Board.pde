class Board{
	int[] bits;
	color[] colors;
	int[] notes;
	int currentAddr;

	int NCELLS;
	int NROWS;
	int NCOLS;
	int NADDRBITS;// Number of address bits
	int NCOLORBITS; // Number of color bits
	int NBITS;

	int totalWidth;
	int totalHeight;

	float cursorX, cursorY, cursorW, cursorH;
	private int t;
	private int txtPtr;
	private String strInstructions;
	private String strToShow;
	private boolean showReceivedInput;

	PFont font;

	Board(){
		NROWS = 8;
		NCOLS = NROWS;
		NCELLS = NROWS * NCOLS;
		NADDRBITS = 6;
		NCOLORBITS = 3;
		NBITS = NADDRBITS + NCOLORBITS;

		bits = new int[NBITS];
		colors = new color[NCELLS];
		notes = new int[NCELLS];

		for(int i=0; i<NCELLS; i++){
			colors[i] = color(0);	
		}
		currentAddr = 0;

		totalWidth = height*95/100;
		totalHeight = totalWidth;

		restartCursor();

		font = loadFont("FreeMonoBold-24.vlw");
		strInstructions = "> the string of bricks/bits under the light encode an instruction for the computer\n> each brick/bit has two possible states: high (standing) or low (laying)\n> feel free to change the state of the bricks/bits, one brick/bit at a time, to issue a new instruction...\n> ready to receive your input..."+nSpaces(200);
		strToShow = strInstructions;

		showReceivedInput = false;

		txtPtr = 0;


	}

	private void restartCursor(){
		t = 0;
		cursorX = 0;
		cursorY = 0;
		cursorW = totalWidth;
		cursorH = totalHeight;
	}

	private int bitsToInt(int[] b){
		int n = 0;
		int p = 0;
		for(int i=b.length-1; i>=0; i--){
			n += b[i] * pow(2,p);
			p++;
		}
		return n;

	}

	private String bitsToString(int[] b){
		String s = "";
		for(int i=0; i<b.length; i++){
			s += b[i];
			if(i%3==2){
				s+= " ";
			}
		}
		return s;

	}

	private int addressFromBits(int[] b){
		int[] a = new int[NADDRBITS];
		for(int i=0; i<NADDRBITS; i++){
			a[i] = b[i];
		}
		return bitsToInt(a);
	}

	private color colorFromBits(int[] b){
		int [] c = new int[NCOLORBITS];
		for(int i=0; i<NCOLORBITS; i++){
			c[i] = b[i+NADDRBITS];
		}
		// Hardcoding the color conversion
		float re = map(c[0], 0, 1, 0, 255);
		float gr = map(c[1], 0, 1, 0, 255);
		float bl = map(c[2], 0, 1, 0, 255);

		return color(re,gr,bl);

	}

	private int colorIntFromBits(int [] b){
		int [] c = new int[NCOLORBITS];
		for(int i=0; i<NCOLORBITS; i++){
			c[i] = b[i+NADDRBITS];
		}
		return bitsToInt(c);
	}

	public int[] bitsToThreeNumbers(){
		int a = addressFromBits(bits);
		int c = colorIntFromBits(bits);
		return new int[]{(a>>3)&0x7, a&0x7, c};
	}



	void clearColors(){
		for(int i=0; i<NCELLS; i++){
			colors[i] = color(0);	
		}
	}

	void setBits(int[] b){
		for(int i=0; i<NBITS; i++){
			bits[i] = b[i];
		}
	}

	private String nSpaces(int n){
		String s = "";
		for(int i=0; i<n; i++){
			s+=" ";
		}
		return s;
	}


	void update(){
		color c = colorFromBits(bits);
		int addr = addressFromBits(bits);
		currentAddr = addr;
		colors[addr] = c;
		notes[addr] = colorIntFromBits(bits);

		restartCursor();

		int [] n = bitsToThreeNumbers();
		showReceivedInput = true;
		txtPtr = 0;
		strToShow = String.format("> received new input: \n%s \n(%d, %d, %d)...%s",bitsToString(bits),n[0],n[1],n[2], nSpaces(120));
	}


	void draw(){
		background(0);
		
		textFont(font,20);
		text("change-a-bit", 20, 40, 180,500);
		text(strToShow.substring(0,txtPtr), 20, 60, 180, 500);


		if(txtPtr == strToShow.length()-1 && showReceivedInput){
			txtPtr = (txtPtr + 1) % strToShow.length();
			strToShow = strInstructions;
			showReceivedInput = false;
		}
		else{
			txtPtr = (txtPtr + 1) % strToShow.length();
		}



		pushMatrix();
		translate((width-totalWidth)/2,(height-totalHeight)/2);
		int row, col, x,y, w, h;

		w = totalWidth/NCOLS;
		h = totalHeight/NROWS;

		noStroke();
		int note;
		for(int i=0; i<NCELLS; i++){
			row = i/NCOLS;		
			col = i%NCOLS;
			x = col*w;
			y = row*h;
			note = notes[i];
			//fill(colors[i]);
			fill( (note>>2) * 255);
			rect(x,y,w,h);

			fill( ((note>>1)&1) * 255);
			rect(x+w/5, y+h/5, w*3/5, h*3/5);

			fill( ((note>>0)&1) * 255);
			rect(x+w*2/5, y+h*2/5, w/5, h/5);
		}

		int i = currentAddr;
		row = i/NCOLS;		
		col = i%NCOLS;
		x = col*w;
		y = row*h;
		t++;
		float a = sin(t*TWO_PI/20);
		strokeWeight(5);
		stroke(map(a,-1,1,0,255));
		noFill();

		float lerpConstant = 0.3;
		cursorX = lerp(cursorX, x, lerpConstant);
		cursorY = lerp(cursorY, y, lerpConstant);
		cursorW = lerp(cursorW, w, lerpConstant);
		cursorH = lerp(cursorH, h, lerpConstant);
		rect(cursorX,cursorY,cursorW,cursorH);

		strokeWeight(3);
		stroke(255);
		rect(0,0,totalWidth,totalHeight);

		popMatrix();
	}

	/*
	String getNote(int i){
		int n = notes[i];
		String [] notes = {"D3","E3","F3"

	}
	*/


}
