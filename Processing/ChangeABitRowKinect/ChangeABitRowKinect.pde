// Kinect
import org.openkinect.freenect.*;
import org.openkinect.processing.*;

// kinectera and OpenCV
import processing.video.*;
import gab.opencv.*;
import java.awt.Rectangle;

// From Greg Borenstein WarpPerspective Example
import org.opencv.imgproc.Imgproc;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.core.Mat;
import org.opencv.core.CvType;

OpenCV opencv, opencv2, opencvFlow, opencvDepth, opencv2Depth;
ArrayList<Contour> contours;

Kinect kinect;

ArrayList<PVector> points;
int selectedPoint;

PImage res;

PImage area, areaDepth;

PImage depthFiltered;

int thr;

int NCOLS, NROWS, NCELLS;

int[] bits;
int[] realBits;

int lastStableMillis;
int millisThr;
float flowMag, flowThr;
boolean stable;
boolean newStable;

int totalChanges;

boolean enableBoard;
Board board;
boolean drawBoard;



void setup(){
	size(1280,800);
//	fullScreen();


	// Kinect
	kinect = new Kinect(this);
	kinect.initDepth();
	kinect.initVideo();
	kinect.enableColorDepth(false);

	// OpenCV
	opencv = new OpenCV(this, kinect.width,kinect.height);
//	opencvDepth = new OpenCV(this, kinect.width, kinect.height);
	
	
	millisThr = 500; // Amount of millis to wait "stable" before declaring it
	flowThr = 0.09; // Threshold of optical flow, greater is "not stable"
	thr = 942; // Threshold of the brightness
	NCOLS = 9;
	NROWS = 1;
	NCELLS = NCOLS*NROWS;

	lastStableMillis = 0;
	stable = false;
	newStable = false;
	totalChanges = 0;

	bits = new int[NCELLS];
	realBits = new int[NCELLS];
	clearBits();

	// Warp
	int roiX = 223;
	int roiY = 69;
	int roiL = 214;
	area = createImage(NCOLS*100,NROWS*100,RGB);
	areaDepth = createImage(area.width,area.height,RGB);
	opencv2 = new OpenCV(this,area.width, area.height);
	opencvFlow = new OpenCV(this,area.width, area.height);
	opencv2Depth = new OpenCV(this,area.width, area.height);

	depthFiltered =  createImage(kinect.width, kinect.height,RGB);

	res = createImage(area.width,area.height,RGB);
	selectedPoint = 0;
	points = new ArrayList<PVector>();
	/*
	// Same orientation as kinectera
	points.add(new PVector(roiX+roiL, roiY));
	points.add(new PVector(roiX, roiY));
	points.add(new PVector(roiX, roiY+roiL));
	points.add(new PVector(roiX+roiL, roiY+roiL));
	*/

	// Rotation
	points.add(new PVector(roiX, roiY+roiL));
	points.add(new PVector(roiX+roiL, roiY+roiL));
	points.add(new PVector(roiX+roiL, roiY));
	points.add(new PVector(roiX, roiY));

	board = new Board();
	drawBoard = false;
	enableBoard = false;

}


void draw(){
	background(0);
	strokeWeight(1);

	PVector aveFlow = new PVector();

	int[] rawDepth = kinect.getRawDepth();
	for (int i=0; i < rawDepth.length; i++) {
	    if (rawDepth[i] <= thr) {
	      depthFiltered.pixels[i] = color(255);
	    }
	    else {
      		depthFiltered.pixels[i] = color(0);
    		}
  	}
	depthFiltered.updatePixels();

//		opencv.loadImage(kinect.getVideoImage());
		opencv.loadImage(depthFiltered);
		// Move contents from video into area
		opencv.toPImage(warpPerspective(points, area.width, area.height), areaDepth);

		// Blur identified areas
		opencv2.loadImage(areaDepth);
		opencv2.blur(40);
		areaDepth = opencv2.getOutput();

//		opencv2.gray();
		opencv2.threshold(100);
		res = opencv2.getSnapshot();

		contours = opencv2.findContours(false,false);

		// Process color image
		opencv.loadImage(kinect.getVideoImage());
		opencv.toPImage(warpPerspective(points, area.width, area.height), area);



		opencv2.loadImage(area);
		opencv2.blur(5);
		area = opencv2.getOutput();

		// Calculate optical flow of color image
		opencvFlow.loadImage(area);
		opencvFlow.calculateOpticalFlow();
		aveFlow = opencvFlow.getAverageFlow();
		flowMag = aveFlow.mag();




	// Calculate change in flow
	if(flowMag > flowThr){
		stable = false;
		lastStableMillis = millis();
	}
	else{
		if(!stable && (millis() - lastStableMillis > millisThr)){
			stable = true;
			lastStableMillis = millis();
			newStable = true;
		}
	}

	pushMatrix();
	scale(0.8);


	// Draw kinect
//	image(kinect.getDepthImage(),0,0,kinect.width,kinect.height);
	image(depthFiltered,0,0,kinect.width,kinect.height);
	// Draw wrapped area
	image(areaDepth,0,kinect.height);
	// Draw filtered area
	image(res,0,kinect.height+area.height);


	// Draw quad in kinect
	noFill();
	stroke(0,255,0);
	beginShape();
	for(PVector p: points){
		vertex(p.x,p.y);
	}
	endShape(CLOSE);
	// Draw selected vertex
	PVector sel = points.get(selectedPoint);
	ellipse(sel.x,sel.y,10,10);

	// Move to filtered area
	pushMatrix();
	translate(0, kinect.height+area.height);

	clearBits();

	// Draw contours
	if(contours != null){

		Contour contour;
//		println(contours.size());
		for (int i=0; i<contours.size()-1; i++) {
			contour = contours.get(i);
		    stroke(0, 0, 255);
		    strokeWeight(2);
		    contour.draw();
	
		    stroke(255, 0, 0);
		    /*
		    beginShape();
		    for (PVector point : contour.getPolygonApproximation().getPoints()) {
		      vertex(point.x, point.y);
	    		}
		    endShape();
		    */

		    Rectangle r = contour.getBoundingBox();
		    int inside = insideCell(r);
		    if(inside>=0){
			bits[inside] = 1;
		    }

		    rect(r.x,r.y,r.width, r.height);
	  	}
	}
	



	// Draw cells 
	stroke(0,255,0);
	int x, y, w, h, col, row;
	w = area.width / NCOLS;
	h = area.height / NROWS;
	for(int i=0; i<NCELLS; i++){
		col = i % NCOLS;
		row = i / NCOLS;
		x = col*w;
		y = row*h;

		if(bits[i]==1){
			if(stable){
				fill(0,255,0,100);
			}
			else{
				fill(255,200,0,100);
			}
		}
		else{
			noFill();
		}
		rect(x,y,w,h);

	}
	popMatrix();



	// Write bits string
	fill(255);
	noStroke();
	textSize(50);
	text(getBitsString(realBits), 100, 200); 
	text(flowMag, 100,450);
	text(totalChanges, 100,500);

	popMatrix(); // For the scale

	if(drawBoard){
		board.draw();
	}



	if(newStable){
		if(enableBoard){
			updateRealBits();

			newStable = false;
		}
	}

}

void keyPressed(){
	switch(key){
		// wasd to move vertex, jk to select vertex, g to save them, l to load them
		// nm to adjust lighting threshold
		// b to toggle drawing board
		// SPACEBAR to enable drawing board
		// S to save screenshot
		// c to clear board
		case 'a':
			points.get(selectedPoint).x--;
			break;
		case 'd':
			points.get(selectedPoint).x++;
			break;
		case 'w':
			points.get(selectedPoint).y--;
			break;
		case 's':
			points.get(selectedPoint).y++;
			break;
		case 'j':
			selectedPoint = (selectedPoint-1+points.size())%points.size();
			break;
		case 'k':
			selectedPoint = (selectedPoint+1)%points.size();
			break;
		case 'n':
			thr--;
			println(thr);
			break;
		case 'm':
			thr++;
			println(thr);
			break;
		case 'S':
			saveScreenshot();
			break;
		case 'b':
			drawBoard = !drawBoard;
			println("Drawing board: "+drawBoard);
			break;
		case ' ':
			enableBoard = !enableBoard;
			println("Board enabled? "+enableBoard);
			break;
		case 'g':
			savePoints();
			break;
		case 'l':
			loadPoints();
			break;
		case 'c':
			board.clearColors();
			board.setBits(realBits);
			board.update();
			break;

	}

}

void saveScreenshot(){
			String fecha = String.format("%04d%02d%02d%02d%02d%02d",year(),month(),day(),hour(),minute(),second());
			save("Screenshot-"+fecha+".png");
			println("Screenshot saved");
}

// Load points for the ROI quad
void loadPoints(){
	String[] str = loadStrings("points.txt");
	String s;
	String[] tokens;
	PVector p;
	points.clear();
	for(int i=0; i<str.length; i++){
		s = str[i];	
		println(s);
		tokens = split(s,",");
		p = new PVector(float(tokens[0]), float(tokens[1]));
		points.add(p);
	}

}

// Save points for the ROI quad
void savePoints(){
	String[] str = new String[4];
	PVector p;
	for(int i=0; i<points.size(); i++){
		p = points.get(i);
		str[i] = p.x +", "+p.y;
		println(str[i]);
	}
	saveStrings("points.txt",str);

}

String getBitsString(int[] b){
	String res = "";
	for(int i=0; i<NCELLS; i++){
		res += b[i];
		if(i%NCOLS == NCOLS-1){
			res += "\n";
		}
	}
	return res;

}

boolean areBitsDifferent(){
	for(int i=0; i<NCELLS; i++){
		if(realBits[i] != bits[i]){
			return true;
		}
	}
	return false;
}

void updateRealBits(){
	boolean updateBoard = false;
	if(areBitsDifferent()){
		totalChanges++;
		println("New change");
		updateBoard = true;


	}
	for(int i=0; i<NCELLS; i++){
		realBits[i] = bits[i];
	}
	if(updateBoard){
		board.setBits(realBits);
		board.update();
		int [] n = board.bitsToThreeNumbers();
		println(n[0] + " "+n[1] + " "+n[2]);
		saveScreenshot();
	}

}

void clearBits(){
	for(int i=0; i<NCELLS; i++){
		bits[i] = 0;
	}
}

// returns number of cell if the rectangle is completely inside
int insideCell(Rectangle r){
	for(int i=0; i<NCELLS; i++){
		if(isInside(r,i)){
			return i;
		}
	}
	return -1;
}

boolean isInside(Rectangle r, int index){
	int col = index%NCOLS;
	int row = index/NCOLS;
	int w = area.width/NCOLS;
	int h = area.height/NROWS;
	int x = col*w;
	int y = row*h;

	// Strict version, only when completely inside
//	return (r.x >= x && r.x+r.width<= x+w && r.y >= y && r.y+r.height <= y+h);

	// Non-strict version, is inside when any corner is inside
	return (isInside(r.x, r.y, index) || isInside(r.x+w, r.y, index) || isInside(r.x+w, r.y+h, index) || isInside(r.x, r.y+h, index));
}

boolean isInside(int px, int py, int index){
	int col = index%NCOLS;
	int row = index/NCOLS;
	int w = area.width/NCOLS;
	int h = area.height/NROWS;
	int x = col*w;
	int y = row*h;

	return (px >= x && px<= x+w && py >= y && py <= y+h);
}

// From Greg Borenstein WarpPerspective example
Mat getPerspectiveTransformation(ArrayList<PVector> inputPoints, int w, int h) {
  Point[] canonicalPoints = new Point[4];
  canonicalPoints[0] = new Point(w, 0);
  canonicalPoints[1] = new Point(0, 0);
  canonicalPoints[2] = new Point(0, h);
  canonicalPoints[3] = new Point(w, h);

  MatOfPoint2f canonicalMarker = new MatOfPoint2f();
  canonicalMarker.fromArray(canonicalPoints);

  Point[] points = new Point[4];
  for (int i = 0; i < 4; i++) {
    points[i] = new Point(inputPoints.get(i).x, inputPoints.get(i).y);
  }
  MatOfPoint2f marker = new MatOfPoint2f(points);
  return Imgproc.getPerspectiveTransform(marker, canonicalMarker);
}

Mat warpPerspective(ArrayList<PVector> inputPoints, int w, int h) {
  Mat transform = getPerspectiveTransformation(inputPoints, w, h);
  Mat unWarpedMarker = new Mat(w, h, CvType.CV_8UC1);
  Imgproc.warpPerspective(opencv.getColor(), unWarpedMarker, transform, new Size(w, h));
  return unWarpedMarker;
}
