var NROWS = 16;
var NCOLS = NROWS;
var aBits = 8;
var cBits = 4;
var nBits;
var N;
var colors = [];
var textInput;
var boton;
var boton2;
var boton3;
var boxes = [];
var currentAddr;
var bits;

function setup() {
  createCanvas(400, 400);
  N = NROWS * NCOLS;
  nBits = aBits+cBits;
  for (var i = 0; i < N; i++) {
    colors.push(color(0, 0, 0));
  }
  noStroke();
  var originalInput = "";
  var x = 400;
  var y = height/2;
  currentAddr = 0;
  for(var i=0; i<nBits; i++){
	originalInput += "0";
	boxes.push( createCheckbox('', false) );
	boxes[i].position(x,y);
	boxes[i].changed(boxesChanged);
	x+= 20;
	if(i%4 == 3){
		x = 400;
		y += 20;
	}
  }
  bits = originalInput;
//  textInput = createInput(originalInput);
	/*
  boton = createButton("Update");
  boton.mousePressed(inputChange);
  boton2 = createButton("Sim");
  boton2.mousePressed(simulate);
  boton3 = createButton("Iterate");
  boton3.mousePressed(it);
  */
}

function draw() {
  // background(220);
  var r, c, x, y, w, h;
  strokeWeight(5);
  for (var i = 0; i < N; i++) {
    fill(colors[i]);
    r = floor(i / NCOLS);
    c = i % NCOLS;
    w = width / NCOLS;
    h = height / NROWS;
    x = c * w;
    y = r * h;
    if(currentAddr == i){
	stroke(255);
    }
    else{
	noStroke();
    }
    rect(x, y, w, h);
  }


}

function boxesChanged(){
	var num = "";
	for(var i=0; i<nBits; i++){
		num += (boxes[i].checked()) ? "1" : "0";
	}
//	textInput.value(num);
	bits = num;
	inputChange();

}

function it() {
  iterate();
  inputChange();
}

function iterate() {
  var inp = textInput.value();
  var index = floor(random(inp.length));
  var ch = inp.charAt(index);
    console.log("Inverted " + index);
    ch = (ch == '1') ? '0' : '1';
    inp = inp.substr(0, index) + ch + inp.substr(index + 1);
  textInput.value(inp);
}

function simulate() {
  for (var i = 0; i < 100; i++) {
    iterate();
    inputChange();
  }
}

function inputChange() {
  var inp = bits;//textInput.value();
  var addr = parseInt(inp.slice(0, aBits), 2);
  /*var r = parseInt(inp.slice(aBits,aBits+1),2)*255;
  var g = parseInt(inp.slice(aBits+1,aBits+2),2)*255;
  var b = parseInt(inp.slice(aBits+2,aBits+3),2)*255;
  console.log(addr+": "+r+","+g+","+b);
  colors[addr] = color(r,g,b);*/
	/*
	 // Grayscale
  var col = parseInt(inp.slice(aBits, aBits + cBits), 2);
  colors[addr] = color(map(col, 0, 15, 0, 255));
  */
	// RGB 8 bit
  var r = map(parseInt(inp.slice(aBits, aBits + 1), 2), 0, 1, 0, 255);
  var g = map(parseInt(inp.slice(aBits+1, aBits + 3), 2), 0, 3, 0, 255);
  var b = map(parseInt(inp.slice(aBits+3, aBits + 4), 2), 0, 1, 0, 255);
  console.log(addr+": "+r+","+g+","+b);
  colors[addr] = color(r,g,b);
  currentAddr = addr;

}
